variable "with_policy" {
  description = "If set to `true`, the bucket will be created with a bucket policy."
  default     = true
}

variable "bucket_name" {
  description = "The Name tag to set for the S3 Bucket."
  default     = "state-bucket"
}

variable "bucket_environment" {
  description = "The Environment tag to set for the S3 Bucket."
  default     = "dev"
}

variable "owner" {
  description = "The Owner tag to set for the S3 Bucket."
  default     = "dev"
}

variable "cost_center" {
  description = "The Cost Center tag to set for the S3 Bucket."
  default     = "12345"
}

variable "project" {
  description = "The project/purpose the S3 bucket is used for."
  default     = "demo"
}
