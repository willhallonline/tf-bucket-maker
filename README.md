# TF Bucket Maker

A simple project use Terraform to build S3 buckets in AWS.

This project uses a number of techniques in build:

1. tflint
1. tfsec
1. checkov
1. terraform-compliance
1. opa (Open Policy Agent)